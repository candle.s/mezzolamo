import React from 'react'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'

import GlobalState from './context/GlobalState';

import Home from './components/Home'
import Login from './components/Login'
import DashBroad from './components/DashBroad';

import { ProtectedRoute } from './hoc/ProtectedRouter'

function App() {
  return (
    <Router>
      <GlobalState>
        <Switch>
          <Route path="/" exact component={Home} />
          <Route path="/login" exact component={Login} />
          <ProtectedRoute exact path="/dashbroad" component={DashBroad} />
          <Route path="*" component={() => "404 NOT FOUND"} />
        </Switch>
      </GlobalState>
    </Router>
  )
}

export default App
