import React, { useEffect, useContext } from 'react'
import { withRouter } from 'react-router-dom'
import Styled, { css } from 'styled-components'

import Nav from '../components/Nav'

import userContext from '../context/user-context'
import uiContext from '../context/ui-context'

const Layout = (props) => {
  const context = useContext(userContext)
  const contextUI = useContext(uiContext)

  useEffect(() => {
    context.me()
  }, [])

  return (
    <React.Fragment>
      <ToggleMenu onClick={contextUI.activeMenu} active={contextUI.ui.activeMenu}>
        <div className="line"></div>
        <div className="line"></div>
        <div className="line"></div>
      </ToggleMenu>
      <Nav isNav={props.location.pathname.match("/login") ? true : false} />
    </React.Fragment>
  )
}

const ToggleMenu = Styled.div`
  cursor: pointer;
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  background-color: #fff;
  position: fixed;
  left: 0;
  top: 0;
  width: 50px;
  height: 50px;
  border-radius: 50%;
  transition: 0.4s;
  margin-left: ${props => props.active ? '300px' : '30px'};
  margin-top: 30px;

  .line {
    border-bottom: 2px solid black;
    margin: 3px 0;
    width: 50%;
    transition: 0.4s;
  }

  ${props => props.active ? css`
    .line {
      border-bottom: 3px solid black;
    }
    .line:nth-child(1) {
      transform: rotate(50deg);
      margin: 0;
    }
    .line:nth-child(2) {
      transform: rotate(-50deg);
      margin-top: -3px;
    }
    .line:nth-child(3) {
      display: none;
    }
  ` : null}
`

export default withRouter(Layout)