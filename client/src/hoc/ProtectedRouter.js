import React, { useState, useEffect } from "react"
import { Route, Redirect } from "react-router-dom"

export const ProtectedRoute = ({
  component: Component,
  ...rest
}) => {
  const [loading, setLoading] = useState(true)
  const [user, setUser] = useState({ isAuth: false })

  useEffect(() => {
    async function fetchData() {
      const result = await fetch('http://localhost:3001/user/me', {
        credentials: 'include'
      })
      const response = await result.json()
      if(response.isAuth) {
        setUser(response)
        setLoading(false)
      } else {
        setLoading(false)
      }
    }
    fetchData()
  }, [])

  return (
    <Route
      {...rest}
      render={props => {
        if(loading) {
          return <div>Loading...</div>
        } else {
          if (user.isAuth && user.me.roles.includes('ADMIN')) {
            return <Component {...props} />
          } else {
            return (
              <Redirect
                to={{
                  pathname: "/",
                  state: {
                    from: props.location
                  }
                }}
              />
            )
          }
        }
      }}
    />
  )
}
