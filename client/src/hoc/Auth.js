import React, { useContext } from 'react'
import { Redirect } from 'react-router-dom'
import userContext from '../context/user-context'

export function Auth(WrappedComponent) {
  return function ComponentWithCheckAuth(props) {
    const context = useContext(userContext)
    
    if(!context.user.isAuth) {
      return (
        <WrappedComponent {...props} />
      )
    } else {
      return (
        <Redirect to="/" />
      )
    }
  }
}