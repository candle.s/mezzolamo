import React from 'react'
import Styled from 'styled-components'
import Fade from 'react-reveal/Fade'

import Layout from '../hoc/Layout'
import Tranding from './Tranding'

function Home() {
  return (
    <React.Fragment>
      <Layout />
      <Hero>
        <div className="hero-detail">
          <h1>Mezzolamo</h1>
          <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
        </div>
      </Hero>
      <Tranding />
      <Second>
        <Fade up>
          <div className="second-section">
            <div className="second-left">
              <h2>
                This is a mezzolamo.
              </h2>
            </div>
          </div>
        </Fade>
      </Second>
    </React.Fragment>
  )
}

const Hero = Styled.div`
  width: 100%;
  height: 100vh;
  background-image: url('/bg-hero.jpg');
  background-position: center top;
  background-size: cover;
  background-attachment: fixed;

  .hero-detail {
    width: 50%;
    padding-top: 7rem;
    padding-left: 5rem;

    h1 {
      padding: 2rem 0;
      font-size: 5rem;
    }
    p {
      font-size: 1.4rem;
      line-high: 1.6;
    }
  }
`

const Second = Styled.div`
  height: 100vh;
  background: linear-gradient(to bottom, rgb(0, 0, 0, 0.6), rgb(0, 0, 0, 0.6)), url('/bg-girl.jpg');
  background-attachment: fixed;
  background-position: center top;
  background-size: cover;
  display: flex;
  align-items: center;

  .second-section {
    color: #fff;
    font-size: 3rem;
    width: 1180px;
    margin: 0 auto;
    text-align: center;
    text-transform: uppercase;
  }
`

export default Home