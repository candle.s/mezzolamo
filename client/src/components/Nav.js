import React, { useState, useEffect, useContext } from 'react'
import { withRouter } from 'react-router-dom'
import { Link } from 'react-router-dom'
import Styled, { css } from 'styled-components'

import userContext from '../context/user-context'
import uiContext from '../context/ui-context'

function Nav(props) {
  const context = useContext(userContext)
  const contextUI = useContext(uiContext)
  const [menuSecondLevel, setMenuSecondLevel] = useState(false)

  const handleScroll = () => {
    const scrollY = window.pageYOffset || document.documentElement.scrollTop
    if(scrollY >= 1415) {
      setMenuSecondLevel(true)
    } else {
      setMenuSecondLevel(false)
    }
  }

  useEffect(() => {
    window.addEventListener('scroll', handleScroll, false)
    return function cleanup() {
      window.removeEventListener('scroll', handleScroll, false)
    }
  })

  const renderAuth = () => {
    if(!context.user) {
      return (
        <MenuList>Loading...</MenuList>
      )
    } else {
      if(context.user.isAuth) {
        return (
          <MenuList isSecondLevel={menuSecondLevel} isNav={props.isNav}>
            <Link to="/" onClick={() => context.logout()} style={{ cursor: 'pointer' }}>
              Logout
            </Link>
          </MenuList>
        )
      } else {
        return (
          <MenuList isSecondLevel={menuSecondLevel} isNav={props.isNav}>
            <Link to="/login">
              Login
            </Link>
          </MenuList>
        )
      }
    }
  }

  const color = () => {
    if(props.match.path.search('login') > 0) {
      return 'rgb(255,255,255,1)'
    } else {
      return 'rgb(0, 0, 0, 0.85)'
    }
  }

  return (
    <React.Fragment>
      <Menu isSecondLevel={menuSecondLevel} active={contextUI.ui.activeMenu} color={color()}>
        <MenuList isSecondLevel={menuSecondLevel} isNav={props.isNav}>
          <Link to="/">
            Mezzolamo
          </Link>
        </MenuList>
        <MenuList isSecondLevel={menuSecondLevel} isNav={props.isNav}>
          <Link to="/fashion">
            Fashion
          </Link>
        </MenuList>
        <MenuList isSecondLevel={menuSecondLevel} isNav={props.isNav}>
          <Link to="/beauty">
            Beauty
          </Link>
        </MenuList>
        <MenuList isSecondLevel={menuSecondLevel} isNav={props.isNav}>
          <Link to="/lifestyle">
            Lifestyle
          </Link>
        </MenuList>
        { renderAuth() }
      </Menu>
    </React.Fragment>
  )
}

const secondActive = `
  background-color: rgb(255,255,255,1);
`

const menuActive = `
  color: #000;
`

const Menu = Styled.div`
  width: calc(100% - 80%);
  height: 100vh;
  position: fixed;
  flex-direction: column;
  z-index: 20;
  font-size: 1.8rem;
  background-color: ${props => props.color};
  transition: 0.4s;
  transform: ${props => props.active ? 'translateX(0)' : 'translateX(-400px)'};
  top: 0;
  display: flex;
  justify-content: space-around;
  align-items: center;

  ${props => props.isSecondLevel ? css`${secondActive}` : ''}
`

const MenuList = Styled.span`
  font-weight: bold;
  text-transform: uppercase;

  a {
    color: #fff;
    transition: 0.4s;
    ${props => props.isSecondLevel ? css`${menuActive}` : '' || props.isNav ? css`${menuActive}` : ''}
  }
`

export default withRouter(Nav)