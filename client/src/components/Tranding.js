import React from 'react'
import Styled from 'styled-components'
import { Link } from 'react-router-dom'

function Tranding() {
  return (
    <Section>
      <Header>
        <h2>Tranding</h2>
      </Header>
      <SectionTreanding>
        <SectionBox>
          <SectionCategory>
            <Link to="/">
              Beauty
            </Link>
          </SectionCategory>
          <SectionImage>
            <Link to="/">
              <img src="/bg-hero.jpg" alt="bg" />
            </Link>
          </SectionImage>
          <SectionDetail>
            <Link to="/">
              <span>dpa[dp[adp[s</span>
              <span>$14.99</span>
            </Link>
          </SectionDetail>
        </SectionBox>
        <SectionBox>
          <SectionCategory>
            <Link to="/">
              Fashion
            </Link>
          </SectionCategory>
          <SectionImage>
            <Link to="/">
              <img src="/bg-hero.jpg" alt="bg" />
            </Link>
          </SectionImage>
          <SectionDetail>
            <Link to="/">
              <span>dpa[dp[adp[s</span>
              <span>$14.99</span>
            </Link>
          </SectionDetail>
        </SectionBox>
        <SectionBox>
          <SectionCategory>
            <Link to="/">
              Lifestyle
            </Link>
          </SectionCategory>
          <SectionImage>
            <Link to="/">
              <img src="/bg-hero.jpg" alt="bg" />
            </Link>
          </SectionImage>
          <SectionDetail>
            <Link to="/">
              <span>dpa[dp[adp[s</span>
              <span>$14.99</span>
            </Link>
          </SectionDetail>
        </SectionBox>
      </SectionTreanding>
    </Section>
  )
}

const Section = Styled.div`
  width: 1180px;
  margin: 0 auto;
  height: auto;
  padding: 100px 0;
`

const Header = Styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  position: relative;
  text-align: center;
  
  :before {
    content: '';
    height: 1px;
    border-bottom: 1px solid black;
    width: 100%;
    position: absolute;
    z-index: -1;
  }

  h2 {
    font-size: 1.8rem;
    width: 200px;
    text-transform: uppercase;
    background: #fff;
  }
`

const SectionTreanding = Styled.div`
  padding: 30px 0;
  display: flex;
  justify-content: space-between;
  align-items: center;
`

const SectionBox = Styled.div`
  flex: 0 31%;
`

const SectionCategory = Styled.div`
  padding: 10px 0;
  font-weight: bold;
`

const SectionImage = Styled.div`
  img {
    width: 100%;
    height: auto;
  }
`

const SectionDetail = Styled.div`
  padding: 10px 0;

  a {
    display: flex;
    justify-content: space-between;
  }
`

export default Tranding