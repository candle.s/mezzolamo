import React, { useState, useContext } from 'react'
import Styled from 'styled-components'

import { Auth } from '../hoc/Auth'

import Layout from '../hoc/Layout'
import userContext from '../context/user-context'


function Login(props) {
  const [stateAuth, setStateAuth] = useState('login')
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const [error, setError] = useState('')

  const context = useContext(userContext)

  const submitForm = event => {
    event.preventDefault()
    if(email.length !== 0 && password.length !== 0) {
      setError('')
      stateAuth === 'login' ? context.login(email, password) : context.register(email, password)
    } else if(email.length === 0 || password.length === 0) {
      setError('Please fill email and password.')
    }
  }

  const renderStateAuth = () => {
    switch(stateAuth) {
      case 'login':
        return (
          <AuthDescription>you not register?   
            <span onClick={() => setStateAuth('register')}>register</span>
          </AuthDescription>
        )
      case 'register':
        return (
          <AuthDescription>you already have account?
            <span onClick={() => setStateAuth('login')}>login</span>
          </AuthDescription>
        )
      default:
        return (
          <AuthDescription>you not register?   
            <span onClick={() => setStateAuth('register')}>register</span>
          </AuthDescription>
        )
    }
  }

  return (
    <Section>
      <Layout />
      <form onSubmit={submitForm}>
        <AuthenForm>
          <h2>{stateAuth === 'login' ? 'Login' : 'Register'}</h2>
          <InputGroup>
            <label>Email</label>
            <input type="text" name="email" id="email" onChange={e => setEmail(e.target.value)} />
          </InputGroup>
          <InputGroup>
            <label>Password</label>
            <input type="password" name="password" id="password" onChange={e => setPassword(e.target.value)} />
          </InputGroup>
          {!context.user.isAuth && <span style={{ textAlign: 'center', padding: '10px 0', color: 'red' }}><strong>{context.user.msg}</strong></span>}
          {
            error && <span style={{ textAlign: 'center', padding: '10px 0', color: 'red' }}>{error}</span>
          }
          { renderStateAuth() }
          <button type="submit">Submit</button>
        </AuthenForm>
      </form>
    </Section>
  )
}

const Section = Styled.div`
  background: linear-gradient(to bottom, rgb(0, 0, 0, 0.6), rgb(0, 0, 0, 0.6)), url('/bg-authen.jpg');
  background-position: center top;
  background-size: cover;
  height: 100vh;
  width: 100%;
`

const AuthenForm = Styled.div`
  padding: 20px 40px;
  position: absolute;
  top: 30%;
  left: 60%;
  display: flex;
  flex-direction: column;
  background-color: rgb(58, 58, 58, 0.85);
  width: 30%;
  font-size: 18px;
  border-radius: 4px;
  color: #fff;

  h2 {
    padding: 20px 0;
    text-align: center;
  }

  label,
  h2 {
    text-transform: uppercase;
  }

  input {
    flex: 0 50%;
    font-size: 18px;
    height: 30px;
    border: none;
    border-radius: 4px;
    padding: 0 10px;

    &:active,
    &:focus {
      outline: none;
      border: 1px solid #18d8a9;
    }
  }

  button {
    width: 150px;
    height: 40px;
    margin: 10px auto;
    border: none;
    border-radius: 4px;
    color: #fff;
    background-color: black;
    font-size: 18px;
  }
`

const InputGroup = Styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin: 10px 0;
`

const AuthDescription = Styled.span`
  padding: 20px 0; 
  font-size: 16px

  span {
    cursor: pointer;
    color: #18d8a9; 
    padding-left: 10px; 
    font-weight: bold; 
    font-size: 20px
  }
`

export default Auth(Login)