import React, { useReducer } from 'react'
import { withRouter } from 'react-router-dom'

import userContext from './user-context'
import uiContext from './ui-context'

import { userReducer, LOGIN, REGISTER, LOGOUT, ME } from './userReducer'
import { uiReducer, TOGGLE_MENU } from './uiReducer'

function GlobalState(props) {
  const [userState, dispatch] = useReducer(userReducer, { user: {} })
  const [uiState, uiDispatch] = useReducer(uiReducer, { activeMenu: false })

  const activeMenu = () => {
    uiDispatch({ type: TOGGLE_MENU })
  }

  const login = async (email, password) => {
    const res = await fetch('http://localhost:3001/user/login', {
      method: 'POST',
      credentials: 'include',
      headers: {
        'Content-type': 'application/json'
      },
      body: JSON.stringify({email, password})
    })
    const result = await res.json()

    await dispatch({ type: LOGIN, user: result })
    
    if(result.success) props.history.push('/dashbroad')
  }

  const register = async (email, password) => {
    const res = await fetch('http://localhost:3001/user/register', {
      method: 'POST',
      headers: {
        'Content-type': 'application/json'
      },
      body: JSON.stringify({email, password})
    })
    const result = await res.json()

    await dispatch({ type: REGISTER, user: result })

    if(result.success) window.location.reload('/login')
  }

  const logout = async () => {
    const res = await fetch('http://localhost:3001/user/logout', {
      credentials: 'include'
    })
    const result = await res.json()
    await dispatch({ type: LOGOUT })
    if(result) {
      const baseUrl = window.location.hostname
      window.location.href = `http://${baseUrl}:3000`
    }
  }

  const me = async () => {
    const res = await fetch('http://localhost:3001/user/me', {
      credentials: 'include'
    })
    const result = await res.json()
    await dispatch({ type: ME, user: result })
  }

  return (
    <userContext.Provider value={{
      user: userState.user,
      login: login,
      register: register,
      me: me,
      logout: logout
    }}>
      <uiContext.Provider value={{
        ui: uiState,
        activeMenu: activeMenu
      }}>
        {props.children}
      </uiContext.Provider>
    </userContext.Provider>
  )
}

export default withRouter(GlobalState)