export const TOGGLE_MENU = 'TOGGLE_MENU'

const toggleMenu = (state) => {
  let toogleState = { ...state }
  toogleState.activeMenu = !state.activeMenu
  return { ...toogleState }
}

export const uiReducer = (state, action) => {
  switch(action.type) {
    case 'TOGGLE_MENU':
      return toggleMenu(state)
    default: return state
  }
}