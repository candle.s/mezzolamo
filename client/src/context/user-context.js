import React from 'react'

export default React.createContext({
  user: {},
  login: () => {},
  register: () => {},
  logout: () => {},
  me: () => {}
})