export const LOGIN = 'LOGIN'
export const REGISTER = 'REGISTER'
export const LOGOUT = 'LOGOUT'
export const ME = 'ME'

const login = (user, state) => {
  const updateUser = {...state.user}
  updateUser.email = user.email || null
  updateUser.role = user.role || null
  updateUser.userId = user.userId || null
  updateUser.isAuth = user.success
  updateUser.msg = user.msg || ''
  return { ...state, user: updateUser }
}

const register = (user, state) => {
  const updateUser = {...state.user}
  updateUser.isAuth = user.success
  updateUser.msg = user.msg || ''
  return { ...state, user: updateUser }
}

const me = (user, state) => {
  let updateUser = {...state.user}
  if(user.isAuth) {
    updateUser.email = user.email || null
    updateUser.roles = user.roles || null
    updateUser.userId = user._id || null
    updateUser.isAuth = user.isAuth
  }
  return { ...state, user: updateUser }
}

const logout = state => {
  return state = {}
}

export const userReducer = (state, action) => {
  switch(action.type) {
    case 'LOGIN':
      return login(action.user, state)
    case 'REGISTER':
      return register(action.user, state)
    case 'LOGOUT':
      return logout(state)
    case 'ME':
      return me(action.user, state)
    default: return state
  }
}