const mongoose = require('mongoose')
const userSchema = mongoose.Schema({
  email: {
    type: String,
    required: true
  },
  password: {
    type: String,
    required: true
  },
  address: {
    type: String
  },
  role: [{
    type: String
  }],
  count: {
    type: Number,
    default: 0
  }
}, { timestamps: true })

const user = mongoose.model('USER', userSchema)
module.exports = user