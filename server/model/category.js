const mongoose = require('mongoose')
const categorySchema = mongoose.Schema({
  name: {
    type: String,
    required: true
  }
}, { timestamps: true })

const category = mongoose.model('CATEGORY', categorySchema)
module.exports = category