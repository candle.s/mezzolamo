const mongoose = require('mongoose')
const productSchema = mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  description: {
    type: String
  },
  attributes: {
    color: [{ type: String }],
    size: [{ type: String }]
  },
  price: {
    type: Number,
    default: 0
  },
  qty: {
    type: Number,
    default: 0
  },
  saled: {
    type: Number,
    default: 0
  },
  thumbnail: {
    type: String,
    required: true
  },
  image: [{type: String}]
  // user: {
  //   type: mongoose.Schema.Types.ObjectId,
  //   ref: 'USER'
  // }
}, { timestamps: true })

const product = mongoose.model('PRODUCT', productSchema)
module.exports = product