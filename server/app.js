const express = require('express')
const app = express()
const mongoose = require('mongoose')
const cors = require('cors')
const helmet = require('helmet')
const compression = require('compression')
const bodyParser = require('body-parser')
const cookieParser = require('cookie-parser')
const jwt = require('jsonwebtoken')
const userSchema = require('./model/user')
const createToken = require('./middleware/createtoken')
require('dotenv').config()

const userRoutes = require('./routes/user')
const categoryRoutes = require('./routes/category')
const productRoutes = require('./routes/product')

async function connectDB() {
  try {
    await mongoose.connect(process.env.MONGO_URL, {useNewUrlParser: true})
    console.log('connect')
  } catch(err) {
    console.log(err)
    process.exit(1)
  }
}

connectDB()

app.use(cors({ credentials: true, origin: true }))
app.use(helmet())
app.use(compression())
app.use(cookieParser())
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))

app.use(async (req, res, next) => {
  const accessToken = req.cookies['access-token']
  const refreshToken = req.cookies['refresh-token']
  if(!accessToken && !refreshToken) {
    return next()
  }

  try {
    const data = jwt.verify(accessToken, process.env.ACCESS_TOKEN)
    req.userId = data.id
    return next()
  } catch {}

  if(!refreshToken) {
    return next()
  }

  let data

  try {
    data = jwt.verify(refreshToken, process.env.REFRESH_TOKEN)
  } catch {
    return next()
  }

  const user = await userSchema.findOne({ _id: data.id })
  if(!user || user.count !== data.count) {
    return next()
  }

  const token = createToken(user)
  res.cookie('access-token', token.accessToken, { httpOnly: true, maxAge: 900000 })
  res.cookie('refresh-token', token.refreshToken, { httpOnly: true, maxAge: 604800000 })
  req.userId = user._id

  next()
})

app.use('/user', userRoutes)
app.use('/category', categoryRoutes)
app.use('/product', productRoutes)

app.listen(process.env.PORT, () => {
  console.log('🚀')
})