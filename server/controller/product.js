const productSchema = require('../model/product')

exports.addProduct = async (req, res, next) => {
  const name = req.body.name
  const description = req.body.description
  const attributes = req.body.attributes
  const price = req.body.price
  const qty = req.body.qty
  const thumbnail = req.body.thumbnail
  const image = req.body.image

  const product = new productSchema({
    name,
    description,
    attributes,
    price,
    qty,
    thumbnail,
    image
  })
  const result = await product.save()
  if(!result) return res.status(500).json({ success: false })
  return res.status(201).json({ success: true })
}