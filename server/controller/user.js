const bcrypt = require('bcrypt')
const userSchema = require('../model/user')
const createToken = require('../middleware/createtoken')

exports.userRegister = async (req, res, next) => {
  const email = req.body.email
  const password = req.body.password
  const result = await userSchema.findOne({ email })
  if(result) return res.status(500).json({ success: false, msg: 'email is already.' })
  const hashPassword = await bcrypt.hash(password, 10)
  const user = new userSchema({
    email,
    password: hashPassword,
    role: 'USER'
  })
  const response = await user.save()
  if(response) {
    return res.status(201).json({ success: true })
  }
}

exports.userLogin = async (req, res, next) => {
  const email = req.body.email
  const password = req.body.password
  const user = await userSchema.findOne({ email })
  if(!user) return res.status(404).json({ success: false, msg: 'invalid email or password.' })
  const isMatch = await bcrypt.compare(password, user.password)
  if(!isMatch) {
    return res.status(404).json({ success: false, msg: 'invalid email or password.' })
  } else {
    const { accessToken, refreshToken } = createToken(user)
    res.cookie('access-token', accessToken, { httpOnly: true, maxAge: 900000 })
    res.cookie('refresh-token', refreshToken, { httpOnly: true, maxAge: 604800000 })
    return res.status(200).json({ 
      success: true
    })
  }
}

exports.userLogout = (req, res, next) => {
  if(req.cookies['access-token'] && req.cookies['refresh-token']) {
    res.clearCookie('access-token')
    res.clearCookie('refresh-token')
    return res.status(200).json({ success: true })
  }
}

exports.me = (req, res, next) => {
  if(!req.user) {
    return res.status(200).json({ me: null, isAuth: false })
  }
  return res.status(200).json({
    me: {
      email: req.user.email,
      roles: req.user.role,
      userId: req.user._id
    },
    isAuth: true
  })
}