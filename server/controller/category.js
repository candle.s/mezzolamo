const categorySchema = require('../model/category')

exports.getCategory = async (req, res, next) => {
  const category = await categorySchema.find({})
  const result = []
  category.map(item => {
    const obj = {}
    obj.id = item._id
    obj.name = item.name
    result.push(obj)
  })
  if(!category) return res.status(500).json({ success: false, msg: 'Something wrong.' })
  return res.status(200).json({ category: result })
}

exports.addCategory = async (req, res, next) => {
  const name = req.body.name
  const category = new categorySchema({ name })
  const response = await category.save()
  if(!response) return res.status(500).json({ success: false, msg: 'Something wrong.' })
  return res.status(201).json({ success: true })
}

exports.removeCategory = async (req, res, next) => {
  const id = req.params.id
  const result = await categorySchema.findByIdAndRemove(id)
  if(!result) return res.status(500).json({ success: false, msg: 'Something wrong.' })
  return res.status(200).json({ success: true })
}