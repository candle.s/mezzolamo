const jwt = require('jsonwebtoken')

 const createToken = user => {
  const accessToken = jwt.sign({ id: user._id }, process.env.ACCESS_TOKEN, { expiresIn: '15min' })
  const refreshToken = jwt.sign({ id: user._id, count: user.count }, process.env.REFRESH_TOKEN, { expiresIn: '7d' })
  return { accessToken, refreshToken }
}

module.exports = createToken