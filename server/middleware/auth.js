const userSchema = require('../model/user')

exports.auth = async (req, res, next) => {
  if(!req.userId) {
    req.user = null
  }
  const user = await userSchema.findById(req.userId)
  req.user = user
  return next()
}

exports.authPermission = async (req, res, next) => {
  if(!req.userId) {
    return res.status(401).json({ success: false, msg: 'You are not permitted.' })
  }
  const user = await userSchema.findById(req.userId)
  if(user.role.includes('ADMIN')) {
    return next()
  }
  return res.status(401).json({ success: false, msg: 'You are not permitted.' })
}