const express = require('express')
const router = express.Router()

const {
  addProduct
} = require('../controller/product')

router.post('/add_product', addProduct)

module.exports = router