const express = require('express')
const router = express.Router()

const { auth } = require('../middleware/auth')

const {
  userRegister,
  userLogin,
  userLogout,
  me
} = require('../controller/user')

router.post('/register', userRegister)
router.post('/login', userLogin)
router.get('/logout', userLogout)
router.get('/me', auth, me)

module.exports = router