const express = require('express')
const Router = express.Router()

const { authPermission } = require('../middleware/auth')

const { 
  getCategory, 
  addCategory,
  removeCategory 
} = require('../controller/category')

Router.get('/', authPermission, getCategory)
Router.post('/', authPermission, addCategory)
Router.delete('/:id', authPermission, removeCategory)

module.exports = Router